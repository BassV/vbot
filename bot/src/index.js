// 'npm init --yes'
// 'npm install fb-bot-framework --save'
// 'npm install express --save'
// 'ngrok http 3000' to make the local server run online https://ngrok.com/download
// 'npm install random-words'

var express = require('express');
var app = express();
var FBBotFramework = require('fb-bot-framework');
var RandomWords = require('random-words');

// Initialize
var bot = new FBBotFramework({
  page_token: "EAAUJjzlnZC7ABABvHP7DZBAQe9hDoLQC1GQ2cD2Jtzo6o4LZB6kruHtd1B7V2OufMOz9bStm6ZAOpYOm7qUpc" +
    "dKEZBFMnhEl0viVkbBj8gyesP69UZAHtWOZBYxXPXUat8OFSU5pxWulwai3kzR7Ivpqd1Hzy4zVkhunJ2kflr69QZDZD",
  verify_token: "EAAUJjzlnZC7ABABvHP7DZBAQe9hDoLQC1GQ2cD2Jtzo6o4LZB6kruHtd1B7V2OufMOz9bStm6ZAOpYOm7qUpc" +
    "dKEZBFMnhEl0viVkbBj8gyesP69UZAHtWOZBYxXPXUat8OFSU5pxWulwai3kzR7Ivpqd1Hzy4zVkhunJ2kflr69QZDZD"
});

// Setup Express middleware for /webhook
app.use('/webhook', bot.middleware());

// Setup listener for incoming messages
bot.on('message', function(userID, message){
  var sentence = RandomWords({min: 2, max: message.length * 3}).join(" ");

  var gg = " =D".repeat(message.length / 2);

  if (message.length < 3)
    bot.sendTextMessage(userID, sentence + gg);
  else if (message.length < 5)
    bot.sendTextMessage(userID, sentence + gg);
  else
    bot.sendTextMessage(userID, sentence + gg);
});

app.get('/', function(req, res){
  res.send('Wassap');
});

app.listen(3000);